package server

import (
	"fmt"
	"strings"
	"time"
)

func (g *GIRCd) HandlePing(session *Session, args []string) {
	if !strings.HasPrefix(args[1], ":") {
		args[1] = ":" + args[1]
	}
	session.WriteSN("PONG", session.Server.ServerName, args[1])
}

func (g *GIRCd) HandleNick(session *Session, args []string) {
	nick := strings.TrimPrefix(args[1], ":")

	if strings.ContainsAny(nick, ":") {
		session.WriteSNN(ERR_ERRONEUSNICKNAME, "* :Invalid nickname.")
		return
	}

	if g.IsNickUsable(nick) {
		if session.Valid {
			host := session.FullHostname()
			for _, channel := range g.Channels {
				if channel.IsInChannel(session) {
					channel.WriteAllWithHost("NICK "+nick, host)
				}
			}
		}
		session.Nickname = nick
	} else {
		session.WriteSN(ERR_NICKNAMEINUSE, "*", nick+" :Nickname is already in use.")
	}
}

func (g *GIRCd) HandleJoin(session *Session, args []string) {
	// TODO: move to FindChannel?
	// TODO: return error if invalid prefix
	for _, channelName := range strings.Split(strings.ToLower(args[1]), ",") {
		if !strings.HasPrefix(channelName, "#") {
			session.WriteSNNT(ERR_NOSUCHCHANNEL, channelName, "Invalid channel name.")
			return
		}

		if _, ok := g.Channels[channelName]; !ok {
			g.Channels[channelName] = &Channel{
				Name:     channelName,
				Sessions: make([]*Session, 0),
				Create:   time.Now().Unix(),
			}
		}
		g.Channels[channelName].Join(session)
	}
}

func (g *GIRCd) HandlePart(session *Session, args []string, data string) {
	if c := g.FindChannel(args[1]); c != nil {
		msg := ""
		if len(args) > 2 {
			args = strings.SplitN(data, " ", 3)
			if len(args) == 3 {
				msg = strings.TrimLeft(args[2], ":")
			}
		}
		c.Leave(session, "PART", msg)
	}
}

func (g *GIRCd) HandleNames(session *Session, args []string) {
	if c := g.FindChannel(args[1]); c != nil {
		session.WriteSNN(RPL_NAMREPLY, c.NamesString())
		session.WriteSNNC(RPL_ENDOFNAMES, c, "End of /NAMES list.")
	}
}

func (g *GIRCd) HandleMsg(kind string, session *Session, args []string, data string) {
	target := args[1]
	message := ""
	a := strings.SplitN(data, " ", 3)
	if len(a) == 3 {
		message = a[2]
	}

	if strings.HasPrefix(target, "#") {
		channel := g.FindChannel(target)
		if channel == nil {
			return
		}

		if channel.IsInChannel(session) {
			channel.WriteAllWithHostExcept(fmt.Sprintf("%s %s %s", kind, target, message), session.FullHostname(), session)
		}
	} else {
		t := g.FindSession(target)
		if t == nil {
			session.WriteSNNT(ERR_NOSUCHNICK, target, "No such nick/channel.")
			return
		}
		t.Write(fmt.Sprintf(":%s %s %s %s", session.FullHostname(), kind, target, message))
	}
}

func (g *GIRCd) HandleMode(session *Session, args []string) {
	c := g.FindChannel(args[1])
	if c == nil {
		return
	} else if !c.IsInChannel(session) {
		return
	}

	if len(args) > 2 {
		mode := args[2]
		switch mode {
		case "b":
			session.WriteSNNC(RPL_ENDOFBANLIST, c, "End of channel ban list.")
		}
		return
	}

	session.WriteSNN(RPL_CHANNELMODEIS, c.Name+" "+c.ModesString())
	session.WriteSNNC(RPL_CHANNELCREATED, c, fmt.Sprintf("%d", c.Create))
}

func (g *GIRCd) HandleWho(session *Session, args []string) {
	target := args[1]

	if strings.HasPrefix(target, "#") {
		c := g.FindChannel(target)
		if c == nil {
			return
		} else if !c.IsInChannel(session) {
			return
		}

		for _, s := range c.Sessions {
			session.WriteSNN(RPL_WHOREPLY, c.Name+" "+
				fmt.Sprintf("%s %s %s %s %s %s %s", s.User, s.Host, g.ServerName, s.Nickname, "H", ":0", s.User))
		}
		session.WriteSNNC(RPL_ENDOFWHO, c, "End of /WHO list.")

	}
}

func (g *GIRCd) HandleTopic(session *Session, args []string, data string) {
	target := args[1]
	argCount := len(args)

	if strings.HasPrefix(target, "#") {
		c := g.FindChannel(target)
		if c == nil {
			return
		} else if !c.IsInChannel(session) {
			return
		}

		if argCount > 2 {
			topic := ""
			args = strings.SplitN(data, " ", 3)
			if len(args) == 3 {
				topic = strings.TrimLeft(args[2], ":")
			}

			if topic != "" {
				c.Topic = topic
				c.TopicWho = session.FullHostname()
				c.TopicTime = time.Now().Unix()
				c.WriteAllWithHost(fmt.Sprintf("TOPIC %s :%s", c.Name, topic), session.FullHostname())
			}

		} else {
			c.SendTopic(session)
		}
	}
}
