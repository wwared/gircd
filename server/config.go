package server

type Configuration struct {
	Network string
	Server  string
	Listen  []ListenConfig
	Debug   bool
}

type ListenConfig struct {
	Bind string
	Tls  bool
	Cert string
	Key  string
}
