package server

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type Session struct {
	Nickname  string
	User      string
	Host      string
	Conn      net.Conn
	Server    *GIRCd
	Valid     bool
	Tls       bool
	writeChan chan []byte
	closeChan chan bool
	quit      bool
}

const deadline = time.Minute * 5

func (session *Session) Process() {
	go func() {
		for {
			select {
			case data := <-session.writeChan:
				if err := session.Conn.SetWriteDeadline(time.Now().Add(deadline)); err != nil {
					session.Close()
					return
				}
				_, err := session.Conn.Write(data)
				if err != nil {
					log.Println("error writing data:", err)
					session.Close()
				}
			case <-session.closeChan:
				break
			}
		}
	}()

	buffer := bufio.NewReaderSize(session.Conn, 1024)
	for {
		if err := session.Conn.SetReadDeadline(time.Now().Add(deadline)); err != nil {
			session.Close()
			return
		}
		line, err := buffer.ReadString('\n')
		if err != nil {
			session.Close()
			return
		}

		session.Server.messageChan <- Message{Data: strings.TrimRight(line, "\r\n"), Session: session}
	}
}

func (session *Session) FullHostname() string {
	return fmt.Sprintf("%s!%s@%s", session.Nickname, session.User, session.Host)
}

func (session *Session) WriteIntro() {
	session.Server.Fantasy.WriteFortuneNotice(session)
}

func (session *Session) WriteWelcome() {
	server := session.Server
	session.WriteSNN(RPL_WELCOME, fmt.Sprintf(":Welcome to the %s network! %s", server.Network, session.FullHostname()))
	session.WriteSNN(RPL_YOURHOST, fmt.Sprintf(":Your host is %s, running %s", session.Server.ServerName, VERSION))
	session.WriteSNN(RPL_CREATED, fmt.Sprintf(":This server was created %s", session.Server.Create))
	session.WriteSNN(RPL_MYINFO, fmt.Sprintf("%s %s %s %s %s", server.ServerName, VERSION, UserModes, ChanModes, ChanModesP))
	session.WriteSNN(RPL_ISUPPORT, fmt.Sprintf("NETWORK=%s CASEMAPPING=rfc1459 CHANMODES=%s CHANTYPES=# PREFIX=(ov)@+ FNC :is supported by this server.", server.Network, ChanModesS))
	session.WriteSNN(RPL_ISUPPORT, "MAXCHANNELS=420 MAXTARGETS=420 MODES=15 NICKLEN=32 TOPICLEN=420 AWAYLEN=420 CHANNELLEN=420 KICKLEN=420 MAXBANS=420 :is supported by this server.")
	session.WriteSNN(RPL_MOTDSTART, fmt.Sprintf(":%s message of the day", server.ServerName))
	session.WriteSNN(RPL_MOTD, ":- {insert 256 color ascii here}")
	session.WriteSNN(RPL_ENDOFMOTD, ":end of message of the day.")
}

func (session *Session) WriteSN(numeric string, nick string, data string) {
	if nick == "" {
		nick = "?"
	}
	session.Write(fmt.Sprintf(":%s %s %s %s", session.Server.ServerName, numeric, nick, data))
}

func (session *Session) WriteSNN(numeric string, data string) {
	session.WriteSN(numeric, session.Nickname, data)
}

func (session *Session) WriteSNNT(numeric string, target string, data string) {
	session.WriteSNN(numeric, target+" :"+data)
}

func (session *Session) WriteSNNC(numeric string, channel *Channel, data string) {
	session.WriteSNNT(numeric, channel.Name, data)
}

func (session *Session) Write(data string) {
	if session.Server.Debug {
		log.Printf("[<] [%s] [%s] %s\n", session.Conn.RemoteAddr(), session.Nickname, string(strings.TrimSuffix(data, "\n")))
	}
	select {
	case session.writeChan <- []byte(data + "\r\n"):
		return
	default:
		log.Println("write buffer full!", session.Nickname, session.Conn.RemoteAddr())
		session.CloseConn()
	}
}

func (session *Session) Close() {
	session.closeChan <- true
	session.Server.delSession <- session
	session.CloseConn()
}

func (session *Session) CloseConn() {
	_ = session.Conn.Close()
}
