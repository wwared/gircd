package server

import (
	"crypto/tls"
	"errors"
	"github.com/BurntSushi/toml"
	"log"
	"net"
	"strings"
	"time"
)

const (
	VERSION    = "gIRCd 0.2"
	UserModes  = "aosDRZ"
	ChanModes  = "bgiklmnoprstvz"
	ChanModesP = "bklov"           // channel modes with parameters
	ChanModesS = "b,k,l,gimnprstz" // used in RPL_ISUPPORT
)

// user modes:
// a = administrator
// o = operator
// s = service
// D = deaf
// R = authenticated messages only
// Z = tls connection

// channel modes:
// b = ban
// g = allow anyone to invite
// i = invite only
// k = key
// l = limit
// m = moderate
// n = no external messages
// o = operator
// p = private
// r = authenticated users only
// s = secret
// t = topic lock
// v = voice
// z = tls only

type Message struct {
	Data    string
	Session *Session
}

func NewGIRCd(network string, serverName string, debug bool) *GIRCd {
	return &GIRCd{
		Network:     network,
		ServerName:  serverName,
		Sessions:    make([]*Session, 0),
		Channels:    make(map[string]*Channel, 0),
		Debug:       debug,
		Create:      time.Now(),
		Fantasy:     NewFantasy(),
		newSession:  make(chan *Session, 0),
		delSession:  make(chan *Session, 0),
		messageChan: make(chan Message, 0),
	}
}

type GIRCd struct {
	Network     string
	ServerName  string
	Sessions    []*Session
	Channels    map[string]*Channel
	Debug       bool
	Create      time.Time
	Fantasy     *Fantasy
	newSession  chan *Session
	delSession  chan *Session
	messageChan chan Message
}

func (g *GIRCd) Process() {
	ticker := time.NewTicker(time.Minute * 1)
	for {
		select {
		case session := <-g.newSession:
			log.Println("new session from", session.Conn.RemoteAddr())
			g.Sessions = append(g.Sessions, session)
			go session.Process()
			session.WriteIntro()

		case session := <-g.delSession:
			log.Println("closing session from", session.Conn.RemoteAddr())
			for _, channel := range g.Channels {
				if channel.IsInChannel(session) {
					channel.Leave(session, "QUIT", "disconnected")
				}
			}
			i := 0
			for _, s := range g.Sessions {
				if s != session {
					g.Sessions[i] = s
					i++
				}
			}
			g.Sessions = g.Sessions[:i]

		case message := <-g.messageChan:
			g.ProcessMessage(message)

		case <-ticker.C:
			for _, s := range g.Sessions {
				s.Write("PING " + g.ServerName)
			}
		}
	}
}

func (g *GIRCd) ProcessMessage(message Message) {
	session, data := message.Session, message.Data
	args := strings.Split(data, " ")
	argCount := len(args)

	if g.Debug {
		log.Printf("[>] [%s] [%s] %s\n", session.Conn.RemoteAddr(), session.Nickname, data)
	}

	if argCount > 1 {
		if strings.ToUpper(args[0]) == "PING" {
			g.HandlePing(session, args)
			return
		}
	}

	if !session.Valid {
		if argCount > 1 {
			switch strings.ToUpper(args[0]) {
			case "NICK":
				g.HandleNick(session, args)

			case "USER":
				session.User = args[1]
			}
		}

		if session.User != "" && session.Nickname != "" {
			session.Valid = true
			session.WriteWelcome()
			return
		}
	}

	if session.Valid {
		cmd := strings.ToUpper(args[0])

		switch cmd {
		case "QUIT":
			session.CloseConn()
			return
		case "FORTUNE":
			g.Fantasy.HandleFortune(session, args)
			return
		}

		if argCount > 1 {
			switch cmd {
			case "NICK":
				g.HandleNick(session, args)

			case "JOIN":
				g.HandleJoin(session, args)

			case "PART":
				g.HandlePart(session, args, data)

			case "NAMES":
				g.HandleNames(session, args)

			case "WHO":
				g.HandleWho(session, args)

			case "MODE":
				g.HandleMode(session, args)

			case "TOPIC":
				g.HandleTopic(session, args, data)

			case "PRIVMSG":
				if argCount > 2 {
					g.HandleMsg(cmd, session, args, data)
				}

			case "NOTICE":
				if argCount > 2 {
					g.HandleMsg(cmd, session, args, data)
				}
			}
		}
	}
}

func (g *GIRCd) FindChannel(name string) *Channel {
	if channel, ok := g.Channels[strings.ToLower(name)]; ok {
		return channel
	}
	return nil
}

func (g *GIRCd) IsNickUsable(nick string) bool {
	nick = strings.ToLower(nick)
	for _, session := range g.Sessions {
		if strings.ToLower(session.Nickname) == nick {
			return false
		}
	}
	return true
}

func (g *GIRCd) FindSession(nick string) *Session {
	nick = strings.ToLower(nick)
	for _, session := range g.Sessions {
		if strings.ToLower(session.Nickname) == nick {
			return session
		}
	}
	return nil
}

func (g *GIRCd) Listen(bind string, enableTls bool, certFile string, keyFile string) error {
	var listen net.Listener

	if enableTls {
		cert, err := tls.LoadX509KeyPair(certFile, keyFile)
		if err != nil {
			return err
		}

		config := tls.Config{Certificates: []tls.Certificate{cert}}

		listen, err = tls.Listen("tcp", bind, &config)
		if err != nil {
			return err
		}
	} else {
		var err error
		listen, err = net.Listen("tcp", bind)
		if err != nil {
			return err
		}
	}

	msg := "listening on " + bind
	if enableTls {
		msg += " (tls)"
	}
	log.Println(msg)

	go func() {
		for {
			conn, err := listen.Accept()
			if err != nil {
				log.Println("error accepting session:", err)
				continue
			}

			host := "unknown"
			if enableTls {
				host = "unknown/tls"
			}

			g.newSession <- &Session{
				Valid:     false,
				Conn:      conn,
				Server:    g,
				Host:      host,
				Tls:       enableTls,
				writeChan: make(chan []byte, 4096),
				closeChan: make(chan bool, 4),
				quit:      false,
			}
		}
	}()

	return nil
}

func Start() error {
	var conf Configuration
	if _, err := toml.DecodeFile("gircd.toml", &conf); err != nil {
		return errors.New("error reading configuration file: " + err.Error())
	}

	g := NewGIRCd(conf.Network, conf.Server, conf.Debug)

	for _, l := range conf.Listen {
		if err := g.Listen(l.Bind, l.Tls, l.Cert, l.Key); err != nil {
			return err
		}
	}

	g.Process()
	return nil
}
