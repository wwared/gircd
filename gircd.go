package main

import (
	"gircd/server"
	"log"
)

// TODO: generalize parsing
// TODO: move deadline settings to configuration file
// TODO: move invalid characters to configuration file, and consider other values to filter
// TODO: improve ping system, disconnect clients that don't respond (don't depend on deadline alone)
// TODO: send error responses instead of just returning
// TODO: better logging
// TODO: consider supporting length limits

func main() {
	if err := server.Start(); err != nil {
		log.Fatalln(err)
	}
}
